const array = [3, 6, -2, -5, 7, 3];

const adjacentElement = (inputArray) => {
    const largestProduct = inputArray.reduce((acc, current, index ) => {
        const nextNumber = inputArray[index + 1]
        if (nextNumber === undefined) return acc
        const actualProduct = current * inputArray[index + 1]
        acc = actualProduct > acc || acc === null ? actualProduct : acc
        return acc
    }, 0)
    return largestProduct;
}

console.log(adjacentElement(array))